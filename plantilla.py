import sys

# tenemos una tupla de los formatos
fordered = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")

def lower_than(format1, format2): # comparamos los formatos dentro de la tupla
    return fordered.index(format1) > fordered.index(format2)

def sort_p(formats, p): # la posicion p va pasando usando lower_than
    while p > 0 and lower_than(formats[p], formats[p - 1]):
        formats[p], formats[p - 1] = formats[p - 1], formats[p]
        p -= 1

def sort_formats(formats): # recorremos la tupla siendo i cada elemento y llamamos a sort_p para ordenar
    for i in range(1, len(formats)):
        sort_p(formats, i)
    return formats

def main():
    formats = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")
    print()

if __name__ == '__main__':
    main()
